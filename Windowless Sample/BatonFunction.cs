﻿using HidLibrary;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Windowless_Sample
{
    public class BatonFunction
    {
        private const int VendorId = 0x0;
        private static readonly int[] ProductIds = new[] { 0x0 };
        private static int _currentProductId;
        private static HidDevice _device;
        private static bool _attached;
        public BatonFunction()
        {
            InitBaton();
        }



        private void InitBaton()
        {
            foreach (var productId in ProductIds)
            {
                //_device = HidDevices.Enumerate(VendorId, productId).FirstOrDefault();
                _device = HidDevices.Enumerate(VendorId, productId).Where(d => (ushort)d.Capabilities.UsagePage == 0x89).FirstOrDefault();

                if (_device == null) continue;

                _currentProductId = productId;

                _device.OpenDevice();

                Console.WriteLine("Usage:" + _device.Capabilities.UsagePage);
                _device.WriteFeatureData(new byte[] { 0xff, 0xda });

                _device.Inserted += DeviceAttachedHandler;
                _device.Removed += DeviceRemovedHandler;

                _device.MonitorDeviceEvents = true;

                _device.ReadReport(OnReport);
                break;
            }

            if (_device != null)
            {
                Console.WriteLine("Baton found, press any key to exit.");
                // Console.ReadKey();
                //_device.CloseDevice();
            }
            else
            {
                Console.WriteLine("Could not find a Baton.");
                //  Console.ReadKey();
            }

        }

        public bool HasBaton()
        {
            foreach (var productId in ProductIds)
            {
                //_device = HidDevices.Enumerate(VendorId, productId).FirstOrDefault();
                _device = HidDevices.Enumerate(VendorId, productId).Where(d => (ushort)d.Capabilities.UsagePage == 0x89).FirstOrDefault();

                if (_device == null) continue;

                _currentProductId = productId;

                _device.OpenDevice();

                Console.WriteLine("Usage:" + _device.Capabilities.UsagePage);
                _device.WriteFeatureData(new byte[] { 0xff, 0xda });

                _device.Inserted += DeviceAttachedHandler;
                _device.Removed += DeviceRemovedHandler;

                _device.MonitorDeviceEvents = true;

                _device.ReadReport(OnReport);
                break;
            }

            if (_device != null)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public void StartToScanDevice()
        {
            _device.Write(new byte[] { 0xff, 0xFD });
        }

        public void StopScanningDevice()
        {
            _device.Write(new byte[] { 0xff, 0xed });
        }
        public void DisconnectAllDevices()
        {
            _device.Write(new byte[] { 0xff, 0xda });
        }

        public byte[] BatteryCapacity(byte deviceName = 0)
        {
            _device.Write(new byte[] { 0xff, 0xbc, deviceName });
            return new byte[] { };//_device.Read().Data;
        }

        public byte[] DeviceConnected(byte deviceName = 0)
        {
            _device.Write(new byte[] { 0xff, 0xdc, deviceName });
            return _device.Read().Data;
        }

        public byte[] DeviceDisconnected(byte deviceName = 0)
        {
            _device.Write(new byte[] { 0xff, 0xdd, deviceName });
            return _device.Read().Data;
        }

        public void DisconnectDevice(byte deviceName = 0)
        {
            _device.Write(new byte[] { 0xff, 0xdf, deviceName });
        }

        public void ChangeDeviceUserName(byte deviceName = 0)
        {
            _device.Write(new byte[] { 0xff, 0xcd, deviceName });
        }


        private static void OnReport(HidReport report)
        {
            if (_attached == false) { return; }

            Console.WriteLine(String.Join(",", report.Data));

            if (report.Data.Length >= 4)
            {

            }

            _device.ReadReport(OnReport);
        }

        private static void KeyPressed(int value)
        {
            Console.WriteLine("Button {0} pressed.", value);
        }

        private static void KeyDepressed()
        {
            Console.WriteLine("Button depressed.");
        }

        private static void DeviceAttachedHandler()
        {
            _attached = true;
            Console.WriteLine("Gamepad attached.");
            _device.ReadReport(OnReport);
        }

        private static void DeviceRemovedHandler()
        {
            _attached = false;
            Console.WriteLine("Gamepad removed.");
        }
    }
}
