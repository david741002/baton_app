﻿using System;
using System.Linq;
using HidLibrary;

namespace BatonUsbLibrary
{
    class Program
    {
        private const int VendorId = 0x0;
        private static readonly int[] ProductIds = new[] { 0x0 };
        private static int _currentProductId;
        private static HidDevice _device;
        private static bool _attached;

        static void Main()
        {

            /*BatonFunction baton = new BatonFunction();
            var cap  = baton.BatteryCapacity(0x0);
            baton.DeviceConnected(0);
            var a = 0;
            a = 1;
           // return;
            */


            foreach (var d in HidDevices.Enumerate(VendorId)) {
               Console.WriteLine(  d.Capabilities.UsagePage);
                Console.WriteLine(d);
                Console.WriteLine(d.Attributes);
                Console.WriteLine(d.Description);

            }
            

            foreach (var productId in ProductIds)
            {
                //_device = HidDevices.Enumerate(VendorId, productId).FirstOrDefault();
             //   _device = HidDevices.Enumerate(VendorId, productId).Where(d=>(ushort)d.Capabilities.UsagePage == 0x89).FirstOrDefault();

                _device = HidDevices.Enumerate(VendorId, productId).ToList<HidDevice>()[1];


                if (_device == null) continue;

                _currentProductId = productId;

                _device.OpenDevice();

                //Console.WriteLine("Usage:"+_device.Capabilities.UsagePage);
                //_device.WriteFeatureData(new byte[] {0xff,0xda });
               // HidReport hidReport = new HidReport(1);
               // hidReport.Data = new byte[] { 0xff, 0xda };
               // _device.WriteReport(hidReport);

                _device.Inserted += DeviceAttachedHandler;
                _device.Removed += DeviceRemovedHandler;

                _device.MonitorDeviceEvents = true;

                _device.ReadReport(OnReport);
                break;
            }

            if (_device != null)
            {
                Console.WriteLine("Baton found, press any key to exit.");
                Console.ReadKey();
                _device.CloseDevice();
            }
            else
            {
                Console.WriteLine("Could not find a baton.");
                Console.ReadKey();
            }
        }

        private static void OnReport(HidReport report)
        {
            if (_attached == false) { return; }
            //_device.Write(new byte[] { 0x30 });
            Console.WriteLine(String.Join(",", report.Data));
            Console.WriteLine(report.ReportId);

            if (report.Data.Length >= 4)
            {
               /*
                var message = MessageFactory.CreateMessage(_currentProductId, report.Data);
                if (message.Depress) { KeyDepressed(); }
                else if (message.MultiplePressed == false)
                {
                    if (message.UpPressed) { KeyPressed(1); }
                    if (message.DownPressed) { KeyPressed(2); }
                    if (message.LeftPressed) { KeyPressed(3); }
                    if (message.RightPressed) { KeyPressed(4); }
                    if (message.Button1Pressed) { KeyPressed(5); }
                    if (message.Button2Pressed) { KeyPressed(6); }
                    if (message.Button3Pressed) { KeyPressed(7); }
                    if (message.Button4Pressed) { KeyPressed(8); }
                    if (message.Button5Pressed) { KeyPressed(9); }
                    if (message.Button6Pressed) { KeyPressed(10); }
                    if (message.Button7Pressed) { KeyPressed(11); }
                    if (message.Button8Pressed) { KeyPressed(12); }
                    if (message.Button9Pressed) { KeyPressed(13); }
                    if (message.Button10Pressed) { KeyPressed(14); }
                }*/
            }

            _device.ReadReport(OnReport);
        }

        private static void KeyPressed(int value)
        {
            Console.WriteLine("Button {0} pressed.", value);
        }

        private static void KeyDepressed()
        {
            Console.WriteLine("Button depressed.");
        }

        private static void DeviceAttachedHandler()
        {
            _attached = true;
            Console.WriteLine("Gamepad attached.");
            _device.ReadReport(OnReport);
        }

        private static void DeviceRemovedHandler()
        {
            _attached = false;
            Console.WriteLine("Gamepad removed.");
        }
    }
}
