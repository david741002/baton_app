﻿using MahApps.Metro.Controls;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;

using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using LibUsbDotNet; // for USB HID bulk
using LibUsbDotNet.LibUsb;
using LibUsbDotNet.Main;
using Windowless_Sample.UIComponents;

namespace Windowless_Sample
{

    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : MetroWindow
    {
        int[] batons = new int[6];
        int receiver_status;
        int x, x_temp = 0;
        Boolean slot_connect = false; //for usb first connect
        public static UsbDevice MyUsbDevice;

        #region SET YOUR USB Vendor and Product ID!

        //public static UsbDeviceFinder MyUsbFinder = new UsbDeviceFinder(0x5986, 0x4028, "GPSTY1100J1190000D0001");
        public static UsbDeviceFinder MyUsbFinder;



        #endregion

        public static string ByteArrayToString(byte[] ba)
        {
            return BitConverter.ToString(ba).Replace("-", "");
        }

        //UWPGlobalVolume.Volume m_volume = new UWPGlobalVolume.Volume();
        BatonFunction batonFunction;
        Thread pbth, page1th_imagechange, page2th_imagechange, page3th_imagechange, scan_batons;

        public MainWindow()
        {
            InitializeComponent();

            NoConnectedDevices.Margin = new Thickness(0, 0, 0, 0);
            batonFunction = new BatonFunction();
            pbth = new Thread(PollingBaton);
            page1th_imagechange = new Thread(Comp1_change);
            pbth.Start();
            page1th_imagechange.Start();
        }

        private void PollingBaton()
        {
            //Thread.Sleep(1000);
            Boolean n = true;
            while (n)
            {
                Thread.Sleep(500);
                Dispatcher.Invoke(() =>
                {
                    //if (batonFunction.HasBaton()) 
                    try
                    {
                        MyUsbFinder = new UsbDeviceFinder(0x5986, 0x4028);
                        MyUsbDevice = UsbDevice.OpenUsbDevice(MyUsbFinder);
                        if (MyUsbDevice != null)
                        {
                            scan_batons = new Thread(ScanBatons);
                            scan_batons.Start();
                            n = false;
                            slot_connect = true;
                            pbth.Abort();
                            Tile_Click_1(null, null);
                        }
                    }
                    catch { }
                });
            }
        }
        private void Comp1_change()
        {

            while (true)
            {
                for (int i = 0; i < 150; i++)
                {
                    Dispatcher.Invoke(() =>
                    {
                        if (i < 10)
                            Comp1.Source = new BitmapImage(new Uri(string.Format("/dongle/Comp 1_0000{0}.png", i), UriKind.Relative));
                        else if (i < 100)
                            Comp1.Source = new BitmapImage(new Uri(string.Format("/dongle/Comp 1_000{0}.png", i), UriKind.Relative));
                        if (i >= 100)
                            Comp1.Source = new BitmapImage(new Uri(string.Format("/dongle/Comp 1_00{0}.png", i), UriKind.Relative));
                    });
                    i++;
                    Thread.Sleep(50);

                }
            }
        }

        private void Pagpressbuttone_change()
        {
            while (true)
            {
                for (int i = 0; i < 50; i++)
                {
                    Dispatcher.Invoke(() =>
                    {
                        if (i < 10)
                            press_button.Source = new BitmapImage(new Uri(string.Format("/press button/press_button_0000{0}.png", i), UriKind.Relative));
                        else
                            press_button.Source = new BitmapImage(new Uri(string.Format("/press button/press_button_000{0}.png", i), UriKind.Relative));
                    });
                    Thread.Sleep(50);
                }
            }
        }

        public float GetVolume()
        {
            try
            {
                //Instantiate an Enumerator to find audio devices
                NAudio.CoreAudioApi.MMDeviceEnumerator MMDE = new NAudio.CoreAudioApi.MMDeviceEnumerator();
                //Get all the devices, no matter what condition or status
                NAudio.CoreAudioApi.MMDeviceCollection DevCol = MMDE.EnumerateAudioEndPoints(NAudio.CoreAudioApi.DataFlow.All, NAudio.CoreAudioApi.DeviceState.All);
                //Loop through all devices
                foreach (NAudio.CoreAudioApi.MMDevice dev in DevCol)
                {
                    try
                    {
                        if (dev.State == NAudio.CoreAudioApi.DeviceState.Active)
                        {

                            //Set at maximum volume
                            return dev.AudioEndpointVolume.MasterVolumeLevelScalar * (float)100;

                        }
                        else
                        {
                        }
                    }
                    catch (Exception ex)
                    {
                    }
                }
            }
            catch (Exception ex)
            {
            }

            return 0;
        }

        public void SetVolume(int level)
        {
            try
            {
                //Instantiate an Enumerator to find audio devices
                NAudio.CoreAudioApi.MMDeviceEnumerator MMDE = new NAudio.CoreAudioApi.MMDeviceEnumerator();
                //Get all the devices, no matter what condition or status
                NAudio.CoreAudioApi.MMDeviceCollection DevCol = MMDE.EnumerateAudioEndPoints(NAudio.CoreAudioApi.DataFlow.All, NAudio.CoreAudioApi.DeviceState.All);
                //Loop through all devices
                foreach (NAudio.CoreAudioApi.MMDevice dev in DevCol)
                {
                    try
                    {
                        if (dev.State == NAudio.CoreAudioApi.DeviceState.Active)
                        {
                            var newVolume = (float)Math.Max(Math.Min(level, 100), 0) / (float)100;

                            //Set at maximum volume
                            dev.AudioEndpointVolume.MasterVolumeLevelScalar = newVolume;

                            dev.AudioEndpointVolume.Mute = level == 0;

                            //Get its audio volume
                            //   _log.Info("Volume of " + dev.FriendlyName + " is " + dev.AudioEndpointVolume.MasterVolumeLevelScalar.ToString());
                        }
                        else
                        {
                            //  _log.Debug("Ignoring device " + dev.FriendlyName + " with state " + dev.State);
                        }
                    }
                    catch (Exception ex)
                    {
                        //Do something with exception when an audio endpoint could not be muted
                        //   _log.Warn(dev.FriendlyName + " could not be muted with error " + ex);
                    }
                }
            }
            catch (Exception ex)
            {
                //When something happend that prevent us to iterate through the devices
                //  _log.Warn("Could not enumerate devices due to an excepion: " + ex.Message);
            }
        }

        private async void Slider_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            //bool result = await m_volume.SetVolume((float)(e.NewValue / 100.0));
            return;
        }

        private void VolCtrl_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
        }

        private void BriCtrl_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            //  Brightness.SetBrightness((short)BriCtrl.Value);
        }

        private void SpeedCtrl_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
        }

        /*private void member_device_show(object sender, MouseButtonEventArgs e)
        {
            member_device member_Device = new member_device();
            member_Device.Show();
            member_Device.Left = this.Left;
            member_Device.Top = this.Top;
            this.Close();
        }*/

        private void Image_MouseDown(object sender, MouseButtonEventArgs e)
        {
            SystemSettings systemSettings = new SystemSettings();

            systemSettings.Show();
            systemSettings.Left = this.Left;
            systemSettings.Top = this.Top;
            this.Close();
        }

        private void ScanBatons()
        {

            Boolean n = true;
            //while (n)
            //{
            Thread.Sleep(100);
            ErrorCode ec = ErrorCode.None;
            Boolean y = false;
            IUsbDevice wholeUsbDevice = MyUsbDevice as IUsbDevice;
            if (!ReferenceEquals(wholeUsbDevice, null))
            {
                // This is a "whole" USB device. Before it can be used, 
                // the desired configuration and interface must be selected.
                // Select config #1
                wholeUsbDevice.SetConfiguration(1);
                // Claim interface #0.
                wholeUsbDevice.ClaimInterface(0);
            }
            // open read endpoint 1.
            /*if (slot_connect == true)
            {
                UsbEndpointWriter write = MyUsbDevice.OpenEndpointWriter(WriteEndpointID.Ep02);
                {
                byte[] writeBufferA = new byte[1024];
                byte[] writeBufferB = new byte[1024];
                writeBufferA[0] = 0x02;
                writeBufferA[1] = 0xFF;
                writeBufferA[2] = 0xDF;
                writeBufferB[0] = 0x02;
                writeBufferB[1] = 0xFF;
                writeBufferB[2] = 0xFD;

                //while (ec == ErrorCode.None)
                    //{
                        int bytesWrite;
                        ec = write.Write(writeBufferA, 5000, out bytesWrite);
                        Thread.Sleep(500);
                        ec = write.Write(writeBufferB, 5000, out bytesWrite);
                        slot_connect = false;
                   // }
                }
            }*/




            UsbEndpointReader reader = MyUsbDevice.OpenEndpointReader(ReadEndpointID.Ep01);
            byte[] readBuffer = new byte[1024];
            while (ec == ErrorCode.None)
            {
                int bytesRead;

                // If the device hasn't sent data in the last 5 seconds,
                // a timeout error (ec = IoTimedOut) will occur. 
                ec = reader.Read(readBuffer, 5000000, out bytesRead);

                if (bytesRead == 0) throw new Exception(string.Format("{0}:No more bytes!", ec));
                // Console.WriteLine("{0} bytes read", bytesRead);

                byte[] output = new byte[bytesRead];
                Array.Copy(readBuffer, output, bytesRead);

                // Write that output to the console.
                Console.WriteLine(ByteArrayToString(output));

                /* if(bytesRead >= 6)
                     Console.WriteLine($"x: {output[4]},y: {output[5]}");*/


                if (output[0] == 0x02)
                    Console.WriteLine($"x: {output[2]},y: {output[3]}");
                for (int i = 0; i < 6; i++)
                {
                    if (output[0] == 0x02 && i == 0)
                        batons[i] = output[0];
                    else if (output[0] == 0x03 && batons[i] == 0)
                    {
                        if (i == 0)
                            batons[i] = output[3];
                        else if (batons[i - 1] != output[3])
                            batons[i] = output[3];
                        else
                            i = 6;
                    }

                }
                receiver_status = output[2];
                //receiver_status = output[0];
                if (receiver_status == 0xED) // stop scan
                {
                    Tile_Click_2(null, null);
                }
                else if (receiver_status == 0xDC) // stop scan
                {
                    Console.WriteLine($"user name: {output[6]},{output[7]},{output[8]},{output[9]},{output[10]}");
                }
                /*else if (receiver_status == 0x3)
                {
                    //x = (int)GetVolume()+output[7];
                    
                   // x = (int)GetVolume()+(output[5]-x_temp);
                    //SetVolume((int)x);
                    //x_temp = output[5];
                }
                else if (receiver_status == 0x33)
                {
                   // x = (int)GetVolume() - output[5];
                   // SetVolume((int)x);
                }*/



            }
            Dispatcher.Invoke(() =>
                {
                    /*MyUsbDevice = UsbDevice.OpenUsbDevice(MyUsbFinder);
                    if (MyUsbDevice != null)
                    {
                        n = false;
                        pbth.Abort();
                        Tile_Click_1(null, null);
                    }*/
                });
            //}
        }

        int sensorLength = 0;
        private void TextBlock_MouseDown(object sender, MouseButtonEventArgs e)
        {



            //if (sensorLength < 6)
            for (sensorLength = 1; sensorLength < 7; sensorLength++)
            {
                //return;

                //sensorLength++;
                if (batons[sensorLength - 1] != 0)
                {
                    int offset = 60 * sensorLength;

                    /*if (sensorLength == 1)
                        AddConductor();
                    else
                        AddMember();*/

                    Separator separator2 = new Separator() { HorizontalAlignment = HorizontalAlignment.Left, Height = 31, Margin = new Thickness(0, 75 + offset, 0, 0), VerticalAlignment = VerticalAlignment.Top, Width = 350, Background = new SolidColorBrush(Color.FromArgb(0xff, 0x64, 0x64, 0x64)) };
                    GlobalGrid.Children.Add(separator2);

                    //PND1.Margin = new Thickness(10, 50 + offset, 365, 570 - offset);
                    //PND2.Margin = new Thickness(310, 50 + offset, 370, 593 - offset);
                    //PND3.Margin = new Thickness(0, 75 + offset, 0, 0);
                }
            }

            TutorialDialog dialog = new TutorialDialog();
            dialog.ShowDialog();

        }

        // TODO: 產生隨機名稱用, 之後應移除
        private static Random random = new Random();

        private void PairNewDeviceMouseDown(object sender, MouseButtonEventArgs e)
        {
            // TODO: 產生隨機名稱用, 之後應移除
            const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            string randomName = new string(Enumerable.Repeat(chars, 8).Select(s => s[random.Next(s.Length)]).ToArray());
            // TODO: 產生隨機名稱用, 之後應移除
            DeviceSelectControl deviceSelectControl = new DeviceSelectControl();
            deviceSelectControl.OnSelected += DeviceSelectControlItemOnSelected;
            deviceSelectControl.DeviceName = randomName;
            if (DeviceSelecter.Children.Count == 0)
                deviceSelectControl.IsConductor = true;
            DeviceSelecter.Children.Add(deviceSelectControl);
        }

        private void DeviceSelectControlItemOnSelected(object sender, RoutedEventArgs e)
        {
            foreach(Object controlItem in DeviceSelecter.Children)
            {
                if (controlItem is DeviceSelectControl && controlItem != sender) // 取消其餘選擇狀態
                    ((DeviceSelectControl)controlItem).IsSelected = false;
            }
        }

        private void Tile_Click_1(object sender, RoutedEventArgs e)
        {
            NoConnectedDevices.Margin = new Thickness(-1000, 0, 344, 0);
            NoConnectedDevicesPlugIn.Margin = new Thickness(0, 0, 0, 0);

            page1th_imagechange.Abort();
            page2th_imagechange = new Thread(Pagpressbuttone_change);
            page2th_imagechange.Start();





            //  while (batonFunction.HasBaton() == false) {  }
        }

        private void Tile_Click_2(object sender, RoutedEventArgs e)
        {

            page2th_imagechange.Abort();
            Thread th = new Thread(ParingFlow);
            th.Start();
            //NoConnectedDevicesPlugIn.Margin = new Thickness(-2000, 0, 0, 0);
            // Paring.Margin = new Thickness(0, 0, 0, 0);


            // TextBlock_MouseDown(sender,null);



            //Thread.Sleep(2000);


            //Thread.Sleep(2000);

        }

        private void Tile_Click_3(object sender, RoutedEventArgs e)
        {
            Tile_Click_2(null, null);
        }


        private void ParingFlow()
        {
            Dispatcher.Invoke(() =>
            {
                Paring.Margin = new Thickness(0, 0, 0, 0);
            });

            // 部必要的假等待
            //Thread.Sleep(3 * 1000);

            Dispatcher.Invoke(() =>
            {
                Paring.Margin = new Thickness(-1000, 0, 344, 0);


                NoConnectedDevicesPlugIn.Margin = new Thickness(-1000, 0, 344, 0);

                ConnectedDevices.Margin = new Thickness(-7, 0, 344, 0);
            });

            //Thread.Sleep(3 * 1000);

            Dispatcher.Invoke(() =>
            {
                ConnectedDevices.Margin = new Thickness(-1000, 0, 344, 0);
                TextBlock_MouseDown(null, null);
            });
        }


    }
}
