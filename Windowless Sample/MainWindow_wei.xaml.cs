﻿using MahApps.Metro.Controls;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Windowless_Sample
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MetroWindow_wei : MetroWindow
    {
        //UWPGlobalVolume.Volume m_volume = new UWPGlobalVolume.Volume();
        public MetroWindow_wei()
        {
            InitializeComponent();

        }

        private void Tile_Click(object sender, RoutedEventArgs e)
        {
            MainWindow m = new MainWindow();
            m.Show();
            m.Left = this.Left;
            m.Top = this.Top;
            this.Close();
        }

        private void Image_MouseDown(object sender, MouseButtonEventArgs e)
        {
            MainWindow m = new MainWindow();
            m.Show();
            m.Left = this.Left;
            m.Top = this.Top;
            this.Close();
        }

        private void Image_MouseDown_1(object sender, MouseButtonEventArgs e)
        {
            SystemSettings systemSettings = new SystemSettings();

            systemSettings.Show();
            systemSettings.Left = this.Left;
            systemSettings.Top = this.Top;
            this.Close();
        }
    }
}
