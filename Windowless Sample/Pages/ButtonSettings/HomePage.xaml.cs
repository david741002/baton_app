﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Windowless_Sample.Pages.ButtonSettings
{
    /// <summary>
    /// HomePage.xaml 的互動邏輯
    /// </summary>
    public partial class HomePage : Page
    {
        public HomePage()
        {
            InitializeComponent();
        }

        private void SpotlightLaserAndMagnifierButtonClick(object sender, MouseButtonEventArgs e)
        {
            Frame frame = (Frame) Window.GetWindow(this).FindName("ButtonSettingFrame");
            frame.Navigate(new Uri("Pages/ButtonSettings/SpotlightLaserAndMagnifierPage.xaml", UriKind.Relative));
        }

        private void ConductorTransferButtonClick(object sender, MouseButtonEventArgs e)
        {
            List<String> deviceNames = new List<string> { "Another Baton 1", "Another Baton 2", "Another Baton 3", "Another Baton 4", "Another Baton 5", };
            TransferConductorDialog dialog = new TransferConductorDialog(deviceNames);
            dialog.ShowDialog();
        }
    }
}
