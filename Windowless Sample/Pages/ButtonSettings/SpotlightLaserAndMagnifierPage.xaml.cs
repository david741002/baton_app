﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Windowless_Sample.Pages.ButtonSettings
{
    /// <summary>
    /// SpotlightLaserAndMagnifierPage.xaml 的互動邏輯
    /// </summary>
    public partial class SpotlightLaserAndMagnifierPage : Page
    {
        public SpotlightLaserAndMagnifierPage()
        {
            InitializeComponent();
        }

        private void BackButtonClick(object sender, MouseButtonEventArgs e)
        {
            Frame frame = (Frame)Window.GetWindow(this).FindName("ButtonSettingFrame");
            NavigationService service = frame.NavigationService;
            if (service.CanGoBack)
                service.GoBack();
        }

        private void LaserButtonClick(object sender, MouseButtonEventArgs e)
        {
            Frame frame = (Frame)Window.GetWindow(this).FindName("ButtonSettingFrame");
            frame.Navigate(new Uri("Pages/ButtonSettings/LaserSettingPage.xaml", UriKind.Relative));
        }
    }
}
