﻿using MahApps.Metro.Controls;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Windowless_Sample
{
    /// <summary>
    /// TransferConductorDialog.xaml 的互動邏輯
    /// </summary>
    public partial class TransferConductorDialog : MetroWindow
    {
        private int _selectedIndex = -1;
        public int SelectedIndex
        {
            get { return _selectedIndex; }
        }

        public TransferConductorDialog(List<String> deviceNames)
        {
            AllowsTransparency = true;
            InitializeComponent();
            deviceListBox.ItemsSource = deviceNames;
        }

        private void CancleButtonClick(object sender, RoutedEventArgs e)
        {
            DialogResult = false;
            Close();
        }

        private void StackPanel_MouseDown(object sender, MouseButtonEventArgs e)
        {
            foreach(object obj in (sender as StackPanel).Children)
                if (obj is RadioButton)
                    (obj as RadioButton).IsChecked = true;
        }

        private void RadioButton_Checked(object sender, RoutedEventArgs e)
        {
            ConfirmButton.ClearValue(Button.ForegroundProperty);
            ConfirmButton.IsEnabled = true;
            Style style = this.FindResource("TransferConductorDialogButtonStyle") as Style;
            ConfirmButton.Style = style;
            _selectedIndex = Convert.ToInt32((sender as RadioButton).Tag);
        }

        private void ConfirmButton_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = true;
            Close();
        }

    }
}
