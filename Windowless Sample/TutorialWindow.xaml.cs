﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Windowless_Sample
{
    /// <summary>
    /// TutorialWindow.xaml 的互動邏輯
    /// </summary>
    public partial class TutorialWindow : Window
    {
        int page = 1;
        public TutorialWindow()
        {
            InitializeComponent();
            switchPage();
        }

        /* next page */
        private void Next_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (page == 3) {
                // 讓他離開和skip功能一樣
                this.Skip_MouseDown(sender, e);
                return;
            }

            page++;
            this.switchPage(page);
        }

        private void switchPage(int page = 1) {
            _NavigationFrame.Navigate(new Uri($"Pages/Tutorial/Page{page}.xaml", UriKind.Relative));
        }

        private void Pagination_MouseDown(object sender, MouseButtonEventArgs e)
        {
            Image image = (Image)sender;

            if (image.Name == "pagination_page1")
            {
                this.page = 1;
                this.switchPage(1);
            }

            if (image.Name == "pagination_page2") {
                this.page = 2;
                this.switchPage(2);
            }

            if (image.Name == "pagination_page3")
            {
                this.page = 3;
                this.switchPage(3);

                
            }
        }

        private void Frame_Navigated(object sender, System.Windows.Navigation.NavigationEventArgs e)
        {
            BitmapImage Image_PaginationOn = new BitmapImage(new Uri("/Assets/ic-pagination-on.png", UriKind.Relative));
            BitmapImage Image_PaginationOff = new BitmapImage(new Uri("/Assets/ic-pagination-off.png", UriKind.Relative));
           
            pagination_page1.Source = Image_PaginationOff;
            pagination_page2.Source = Image_PaginationOff;
            pagination_page3.Source = Image_PaginationOff;

            if (this.page == 1) {
                pagination_page1.Source = Image_PaginationOn;
            }

            if (this.page == 2)
            {
                pagination_page2.Source = Image_PaginationOn;
            }

            if (this.page == 3)
            {
                pagination_page3.Source = Image_PaginationOn;
            }
        }

        private void Skip_MouseDown(object sender, MouseButtonEventArgs e)
        {
            Application.Current.MainWindow = new MainWindow();
            Application.Current.MainWindow.Show();

            this.Close();
        }
    }
}
