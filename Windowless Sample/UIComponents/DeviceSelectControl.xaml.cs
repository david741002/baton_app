﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Windowless_Sample.UIComponents
{
    /// <summary>
    /// DeviceSelectControl.xaml 的互動邏輯
    /// </summary>
    public partial class DeviceSelectControl : UserControl
    {
        public static readonly RoutedEvent OnSelectedEvent = EventManager.RegisterRoutedEvent(nameof(OnSelected), RoutingStrategy.Tunnel, typeof(RoutedEventHandler), typeof(DeviceSelectControl));

        public event RoutedEventHandler OnSelected
        {
            add
            {
                AddHandler(OnSelectedEvent, value);
            }
            remove
            {
                RemoveHandler(OnSelectedEvent, value);
            }
        }

        public string DeviceName
        {
            get => DeviceNameLable.Content.ToString();
            set
            {
                DeviceNameLable.Content = value;
                NotConductorDeviceNameLable.Content = value;
            }
        }

        public bool IsSelected
        {
            get { return (bool)this.GetValue(IsSelectedProperty); }
            set { this.SetValue(IsSelectedProperty, value); }
        }

        public bool IsConductor
        {
            get { return (bool)this.GetValue(IsConductorProperty); }
            set { this.SetValue(IsConductorProperty, value); }
        }

        public static readonly DependencyProperty IsSelectedProperty = DependencyProperty.RegisterAttached(nameof(IsSelected), typeof(bool), typeof(DeviceSelectControl), new PropertyMetadata(false, IsSelectedPropertyChangedCallback));
        public static readonly DependencyProperty IsConductorProperty = DependencyProperty.RegisterAttached(nameof(IsConductor), typeof(bool), typeof(DeviceSelectControl), new PropertyMetadata(false, IsConductorPropertyChangedCallback));

        public DeviceSelectControl()
        {
            InitializeComponent();
            MouseEnter += new MouseEventHandler(OnMouseEnter);
            MouseLeave += new MouseEventHandler(OnMouseLeave);
        }

        private void OnMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            IsSelected = !IsSelected;
            RaiseEvent(new RoutedEventArgs(OnSelectedEvent));
        }

        void OnMouseLeave(object sender, MouseEventArgs e)
        {
            if (IsSelected) return;
            DeviceSelectControl deviceSelectControl = (DeviceSelectControl)sender;
            var brush = Resources["DefaultBackground"] as SolidColorBrush;
            deviceSelectControl.Background = brush;
        }

        void OnMouseEnter(object sender, MouseEventArgs e)
        {
            if (IsSelected) return;
            var brush = Resources["HoverBackground"] as LinearGradientBrush;
            DeviceSelectControl deviceSelectControl = (DeviceSelectControl)sender;
            deviceSelectControl.Background = brush;
        }

        private static void IsSelectedPropertyChangedCallback(DependencyObject dependencyObject, DependencyPropertyChangedEventArgs args)
        {
            DeviceSelectControl deviceSelectControl = (DeviceSelectControl)dependencyObject;
            if (args.NewValue is bool && bool.Parse(args.NewValue.ToString())) // 選擇
            {
                deviceSelectControl.Background = new SolidColorBrush(Color.FromArgb(255, 0, 195, 135));
            }
            else // 未選擇
            {
                deviceSelectControl.Background = new SolidColorBrush(Color.FromArgb(255, 36, 36, 36));
            }
        }

        private static void IsConductorPropertyChangedCallback(DependencyObject dependencyObject, DependencyPropertyChangedEventArgs args)
        {
            DeviceSelectControl deviceSelectControl = (DeviceSelectControl)dependencyObject;
            if (args.NewValue is bool && bool.Parse(args.NewValue.ToString())) // IsConductor
            {
                deviceSelectControl.NotConductorDeviceNameLable.Visibility = Visibility.Hidden;
                deviceSelectControl.MemberIcon.Visibility = Visibility.Hidden;
                deviceSelectControl.DeviceNameLablePanel.Visibility = Visibility.Visible;
                deviceSelectControl.ConductorIcon.Visibility = Visibility.Visible;
            }
            else
            {
                deviceSelectControl.NotConductorDeviceNameLable.Visibility = Visibility.Visible;
                deviceSelectControl.MemberIcon.Visibility = Visibility.Visible;
                deviceSelectControl.DeviceNameLablePanel.Visibility = Visibility.Hidden;
                deviceSelectControl.ConductorIcon.Visibility = Visibility.Hidden;
            }
        }
    }
}
