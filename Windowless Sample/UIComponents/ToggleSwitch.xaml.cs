﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Windowless_Sample.UIComponents
{
    /// <summary>
    /// ToggleSwitch.xaml 的互動邏輯
    /// </summary>
    public partial class ToggleSwitch : UserControl
    {
        public static readonly RoutedEvent SwitchOpenEvent = EventManager.RegisterRoutedEvent(nameof(OnSwitchOpenEvent), RoutingStrategy.Tunnel, typeof(RoutedEventHandler), typeof(ToggleSwitch));

        public event RoutedEventHandler OnSwitchOpenEvent
        {
            add
            {
                AddHandler(SwitchOpenEvent, value);
            }
            remove
            {
                RemoveHandler(SwitchOpenEvent, value);
            }
        }

        public static readonly RoutedEvent SwitchOffEvent = EventManager.RegisterRoutedEvent(nameof(OnSwitchOffEvent), RoutingStrategy.Tunnel, typeof(RoutedEventHandler), typeof(ToggleSwitch));

        public event RoutedEventHandler OnSwitchOffEvent
        {
            add
            {
                AddHandler(SwitchOffEvent, value);
            }
            remove
            {
                RemoveHandler(SwitchOffEvent, value);
            }
        }

        public static readonly DependencyProperty ToggledProperty = DependencyProperty.Register(
            nameof(Toggled),
            typeof(bool),
            typeof(ToggleSwitch),
            new FrameworkPropertyMetadata(false)
        );

        public bool Toggled;
        public ToggleSwitch()
        {
            InitializeComponent();
        }

        private void OnMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            Toggled = !Toggled;
            if (!Toggled)
                RaiseEvent(new RoutedEventArgs(SwitchOffEvent));
            else
                RaiseEvent(new RoutedEventArgs(SwitchOpenEvent));
        }
    }
}
